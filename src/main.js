import { createApp } from 'vue'
import App from './App.vue'

import VueEasyLightbox from 'vue-easy-lightbox'

// importing AOS
import AOS from 'aos'
import 'aos/dist/aos.css'

const app = createApp(App);

app.use(AOS.init(), VueEasyLightbox);
// or
app.component(VueEasyLightbox.name, VueEasyLightbox)

app.mount('#app')
